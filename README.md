# Brainfuck Runner
Brainfuck language interpreter written in .NET with support of tokenization &amp; code validation

Core logic is encapsulated in [BfEngine] class, it's the entry point to any operation executed on Brainfuck code. [BfEngine] is able to run Brainfuck code in two modes - simple or optimized. Optimized mode uses advanced & faster interpreting process. Validation is another feature of [BfEngine] - it can analyze Brainfuck code for errors with specified level of tolerance. Thus, the maximal tolerance policy will lead to finding errors only when unmatched Brainfuck open/close loop tokens encountered. [BfEngine] is also robust in tokenizing Brainfuck code into tokens - single and block-constucted ones for Brainfuck loops.
